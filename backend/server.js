const app = require("./app")
const connectDB = require("./config/db")

const dotenv = require("dotenv")

// Handle Uncaught Exceptions
process.on("uncaughtException", err => {
  console.log(`ERROR: ${err.stack}`)
  console.log("Shutting down server due to uncaught exception")
  process.exit(1)
})

// Setting up config file
dotenv.config({ path: "backend/config/config.env" })

// Connecting to Database
connectDB()

const server = app.listen(process.env.PORT, () => {
  console.log(
    `Server started on PORT: ${process.env.PORT} in ${process.env.NODE_ENV} mode.`
  )
})

// Unhandled Promise Rejections
process.on("unhandledRejection", err => {
  console.log(`ERROR: ${err.stack}`)
  console.log("Shutting down server due to Unhandled Promise Rejection")
  server.close(() => {
    process.exit(1)
  })
})
