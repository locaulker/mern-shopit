const Product = require("../models/product")
const dotenv = require("dotenv")
const connectDB = require("../config/db")

const products = require("../data/products")

// setting dotenv file
dotenv.config({ path: "backend/config/config.env" })

connectDB()

const seedProducts = async () => {
  try {
    await Product.deleteMany()
    console.log("Products Deleted from DB")

    await Product.insertMany(products)
    console.log("All products added to DB")

    process.exit()
  } catch (error) {
    console.log(error.message)

    process.exit()
  }
}

seedProducts()
