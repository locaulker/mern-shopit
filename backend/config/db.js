const mongoose = require("mongoose")

const connectDB = () => {
  mongoose
    .connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    })
    .then(con => {
      console.log(`MongoDB connected to HOST: ${con.connection.host}`)
    })
}

module.exports = connectDB
